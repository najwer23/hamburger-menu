window.onload = function () {
    const hamburgerBtn = document.querySelector('.hamburgerBtn');

    function menuClick() {
        const menu = document.querySelector('.menu');

        hamburgerBtn.classList.toggle('hamburgerActive');
        hamburgerBtn.setAttribute('aria-expanded', hamburgerBtn.classList.contains('hamburgerActive'));
        menu.classList.toggle('menuActive');
        document.body.classList.toggle('scroll');

        let arrayLinks = document.getElementsByClassName('link');
        for (let i = 0; i < arrayLinks.length; i++) {
            arrayLinks[i].classList.toggle('hideLink');
        }     
    }

    hamburgerBtn.addEventListener('click', menuClick);
}

